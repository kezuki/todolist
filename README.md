# to-do-list

# Client

VueJS 

## Client command
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```
# Server

NodeJs, ExpressJs, Mongoose, MongoDB

## Server command
````
yarn start
