const {Schema, model} = require('mongoose');

let CardSchema = new Schema({
  title: {
    type: String,
    require: true,
  },
  description: String,
  idList: {
    type: Schema.Types.ObjectId,
    ref: 'List'
  },
  created: {
    type: Date,
    default: Date.now,
  },
});


module.exports = model('Card', CardSchema, 'cards');
