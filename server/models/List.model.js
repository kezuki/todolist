const {Schema, model} = require('mongoose');

let ListSchema = new Schema({
  title: {
    type: String,
    require: true,
  },
  cards: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Card',
    },
  ],
  created: {
    type: Date,
    default: Date.now,
  },
});


module.exports = model('List', ListSchema, 'lists');
