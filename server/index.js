const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose')
require('dotenv').config();

const app = express();
const routerTodo = require('./routes')
const PORT = process.env.PORT || 8082;
const DBuri = process.env.ATLAS_URI

app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(cors({
  origin: 'http://localhost:8080',
  method: ['GET', 'POST'],
}));
mongoose.Promise = global.Promise;
mongoose.connect(DBuri, {
    useNewUrlParser: true,
  useUnifiedTopology: true,
  }).then(_ => console.log('Connection MongoDB successfully')
  ).catch(error => console.log('Error MongoDB: ' + error))

app.use(routerTodo);

app.listen(PORT, () => console.log(`App listening on port ${PORT}`));
