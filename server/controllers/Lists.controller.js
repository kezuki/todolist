const {List} = require('../models');

exports.create = (req, res) => {

  if(!req.body.title) {
    return res.status(400).send({message: "Title can not be empty!"})
  }

  const list = new List({
    title: req.body.title,
    cards: req.body.cards || [],
  });

  list
    .save(list)
    .then( data => {
      return res.status(200).send(data);
    })
    .catch(err => {
      return res.status(500).send({
        message: err.message || 'Some error occurred while creating the List.'
      })
    })
};

exports.findAll = (req, res) => {
  List.find()
    .populate('cards')
    .then(data => {
      return res.status(200).send(data);
    })
    .catch(err => {
      return res.status(500).send({
        message: err.message || 'ome error occurred while retrieving List.'
      })
    })
};

