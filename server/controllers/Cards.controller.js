const { Card, List} = require('../models');

exports.create = async(req, res) => {

  const id = await req.body.idList;

  if(!req.body.title) {
   return  res.status(400).send({message: "Title can not be empty!"})
  } else if (!id) {
    return res.status(400).send({message: 'List id can not be empty!'})
  }

  const cardData = new Card({
    title: req.body.title,
    description: req.body.description,
    idList: id
  })


cardData
  .save()
  .then(card => {

    List
      .findById(id)
      .exec((err, list) => {
        if (err) {
          return res.status(400).send({message: err.message || 'Erro add card to list'})
        }

        list.cards.push(card);
        list
          .save()
      })
    return res.status(200).send(card);
  })
  .catch(err => {
    return res.status(500).send({
      message: err.message || 'ome error occurred while retrieving Card.'
    });
  });
};
