const { Router } = require('express');
const router = Router();
const listController = require('../controllers/Lists.controller');
const cardController = require('../controllers/Cards.controller');
//Get
router.get('/lists',listController.findAll);

//Post
router.post('/lists', listController.create);
router.post('/cards', cardController.create)

module.exports = router;