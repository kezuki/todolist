import http from '../http_common';

export const addCard = (card) => http.post('/cards', {...card});
