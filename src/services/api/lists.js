import http from '../http_common';

const listUrl = '/lists';

const getLists = () => http.get(listUrl);
const postList = (data) => http.post(listUrl, {...data});

export {
  getLists,
  postList,
};
