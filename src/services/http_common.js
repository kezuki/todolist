import axios from "axios";

console.log('ENV', process.env)

const httpClient = axios.create({
    baseURL: process.env['VUE_APP_DEV'] || 'http://localhost:8082',
    headers: {
      'Accept': 'application/json',
      'Content-type': 'application/json'
    }
});

export default httpClient;
