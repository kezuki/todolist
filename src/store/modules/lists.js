import {getLists, postList} from '@/services/api/lists';
import {addCard} from '@/services/api/cards';

const listsStore = {
  namespaced: true,
  state: () => {
    return {
      lists: []
    }
  },
  mutations: {
    SET_LIST(state, payload) {
      state.lists = payload;
    },
    UPDATE_LIST(state, payload) {
      state.lists.push(payload);
    },
    ADD_CARD_TO_LIST(state, {data}) {
      state.lists.find(list => list._id === data.idList).cards.push(data)
    },
  },
  getters: {
    getLists: state => state.lists
  },
  actions: {
    async getListsDataAC({commit}) {
      try {
        const response = await getLists();
        commit('SET_LIST', response.data)
      } catch (err) {
        console.log(err)
      }
    },
    async addListsData({commit}, payload) {
      try {
        const response = await postList(payload);

        commit('UPDATE_LIST', response.data)
      } catch (err) {
        console.log( err)
      }

    },
    async addCardToList(context, payload) {
      try {
        const response = await addCard(payload);
        context.commit('ADD_CARD_TO_LIST', response)
      } catch (err) {
        console.log(err)
      }
    }
  },
}


export default listsStore;
