import Vue from 'vue';
import Vuex from 'vuex';
import listsStore from "@/store/modules/lists";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    Lists: listsStore,
  },
});
